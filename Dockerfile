FROM ubuntu:latest

RUN apt-get update && apt-get install -y \
        wget \
        openjdk-8-jre \
        docker.io \
        --no-install-recommends \
        && rm -rf /var/lib/apt/lists/*

RUN wget https://repo.jenkins-ci.org/releases/org/jenkins-ci/plugins/swarm-client/3.9/swarm-client-3.9.jar

COPY entrypoint.sh entrypoint.sh
RUN chmod ugo+x entrypoint.sh

ENV USERNAME=kettunen
ENV PASSWORD=topike12
ENV EXECUTORS=1

ENTRYPOINT [ "/entrypoint.sh" ]