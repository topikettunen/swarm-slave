#!/bin/sh

java -jar swarm-client-3.9.jar -master http://master:8080 -username "$USERNAME" -password "$PASSWORD" -name "$NAME" -executors "$EXECUTORS" -labels $LABELS
